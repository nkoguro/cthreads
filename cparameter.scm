;; -*- coding: utf-8; mode: scheme -*-
;;
;; cparameter.scm - parameter object for cooperative threads
;; 
;;  Copyright (c) 2006 KOGURO, Naoki (naoki@koguro.net)
;;  All rights reserved.
;; 
;;  Redistribution and use in source and binary forms, with or without 
;;  modification, are permitted provided that the following conditions 
;;  are met:
;; 
;;  1. Redistributions of source code must retain the above copyright 
;;     notice, this list of conditions and the following disclaimer.
;;  2. Redistributions in binary form must reproduce the above copyright 
;;     notice, this list of conditions and the following disclaimer in the 
;;     documentation and/or other materials provided with the distribution.
;;  3. Neither the name of the authors nor the names of its contributors 
;;     may be used to endorse or promote products derived from this 
;;     software without specific prior written permission.
;; 
;;  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;; 
;;  $Id$

(define-module cparameter
  (use cthreads)
  (use gauche.hook)

  (export <cparameter> make-parameter parameterize
          parameter-pre-observers
          parameter-post-observers
          parameter-observer-add!
          parameter-observer-delete!))

(select-module cparameter)

(define-class <cparameter> ()
  ((filter :init-keyword :filter)
   (pre-observers :init-form (make-hook 2))
   (post-observers :init-form (make-hook 2))))

(define-method object-apply ((self <cparameter>) . maybe-newval)
  (if (pair? maybe-newval)
      (if (null? (cdr maybe-newval))
          (or (and-let* ((pair (assq self (slot-ref (current-thread) 'params))))
                (let ((new ((slot-ref self 'filter) (car maybe-newval)))
                      (old (cdr pair)))
                  (when (slot-ref self 'pre-observers)
                    (run-hook (slot-ref self 'pre-observers) old new))
                  (set-cdr! pair new)
                  (when (slot-ref self 'post-observers)
                    (run-hook (slot-ref self 'post-observers) old new))
                  old))
              (errorf "the thread ~s doesn't have parameter ~s"
                      (current-thread) self))
          (error "wrong number of arguments for parameter" maybe-newval))
      (or (and-let* ((pair (assq self (slot-ref (current-thread) 'params))))
            (cdr pair))
          (errorf "the thread ~s doesn't have parameter ~s"
                  (current-thread) self))))

(define (make-parameter value . args)
  (let-optionals* args ((filter (lambda (x) x)))
    (let ((param (make <cparameter> :filter filter)))
      (slot-set! (current-thread) 'params
                 (cons (cons param value)
                       (slot-ref (current-thread) 'params)))
      param)))

(define-syntax parameterize
  (syntax-rules ()
    ((_ (binds ...) . body)
     (%parameterize () () () () (binds ...) body))))

(define-syntax %parameterize
  (syntax-rules ()
    ((_ (param ...) (val ...) (tmp1 ...) (tmp2 ...) () body)
     (let ((tmp1 val) ... (tmp2 #f) ...)
       (dynamic-wind
        (lambda () (set! tmp2 (param tmp1)) ...)
        (lambda () . body)
        (lambda () (param tmp2) ...))))
    ((_ (param ...) (val ...) (tmp1 ...) (tmp2 ...) ((p v) . more) body)
     (%parameterize (param ... p) (val ... v) (tmp1 ... tmp1a) (tmp2 ... tmp2a) 
more body))
    ((_ params vals vars other body)
     (syntax-error "malformed binding list for parameterize" other))
    ))

(define-method parameter-pre-observers ((self <cparameter>))
  (slot-ref self 'pre-observers))

(define-method parameter-post-observers ((self <cparameter>))
  (slot-ref self 'post-observers))

(define-method parameter-observer-add! ((self <cparameter>) proc . args)
  (let-optionals* args ((when 'after)
                        (where 'append))
    (unless (memq when '(before after))
      (error "`when' argument of parameter-observer-add! must be either 'before or 'after" when))
    (unless (memq where '(prepend append))
      (error "`where' argument of parameter-observer-add! must be either 'prepend or 'append" when))
    (add-hook! (if (eq? when 'before)
                   (parameter-pre-observers self)
                   (parameter-post-observers self))
               proc
               (eq? where 'append))))

(define-method parameter-observer-delete! ((self <cparameter>) proc . args)
  (let ((where (get-optional args #f)))
    (unless (eq? where 'after)
      (delete-hook! (parameter-pre-observers self) proc))
    (unless (eq? where 'before)
      (delete-hook! (parameter-post-observers self) proc))
    ))

      
(provide "cont-parameter")

