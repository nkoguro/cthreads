;; -*- coding: utf-8; mode: scheme -*-
;;
;; cthreads.scm - cooperative thread library
;;
;;   Copyright (c) 2006 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;
;;   $Id$
;;

(define-module cthreads
  (use srfi-1)
  (use srfi-2)
  (use srfi-19)
  (use util.queue)
  (use gauche.selector)
  
  (export gauche-thread-type
          
          <cthread> current-thread thread? make-thread thread-name 
          thread-specific-set! thread-specific
          thread-start! thread-yield! thread-sleep! 
          thread-terminate! thread-join!
          thread-select!
          
          <cmutex> mutex? make-mutex mutex-name 
          mutex-specific mutex-specific-set!
          mutex-state mutex-lock! mutex-unlock! with-locking-mutex

          <ccondition-variable> condition-variable? make-condition-variable 
          condition-variable-name
          condition-variable-specific condition-variable-specific-set!
          condition-variable-signal! condition-variable-broadcast!
          
          <thread-exception> <join-timeout-exception> <abandoned-mutex-exception>
          <terminated-thread-exception> <uncaught-exception>
          join-timeout-exception? abandoned-mutex-exception?
          terminated-thread-exception? uncaught-exception?
          uncaught-exception-reason))

(select-module cthreads)

;; 
(define (%make-result-value v)
  (cons 'value v))

(define (%make-result-exception e)
  (cons 'exception e))

(define (%result-return result)
  (if (eq? (car result) 'value)
      (cdr result)
      (raise (cdr result))))
      
(define-method absolute-time ((time <time>))
  time)

(define-method absolute-time ((sec <number>))
  (seconds->time (+ (time->seconds (current-time)) sec)))
  
;;
;; scheduler
;;
(define-class <cthread> ()
  ((name :init-keyword :name
         :init-value #f)
   (cont :init-keyword :cont
         :init-value #f)
   (specific :init-value #f)
   (result :init-value #f)
   (params :init-value '()
           :init-keyword :params)))

(define-class <scheduler> ()
  ((thread-queue :init-form (make-queue))
   (runnable-table :init-form (make-hash-table))
   (blocking-table :init-form (make-hash-table))
   (current-thread :init-form (make <cthread> :name "root"))))

(define (%current-thread scheduler)
  (slot-ref scheduler 'current-thread))

(define (%add-runnable-thread! scheduler thread)
  (enqueue! (slot-ref scheduler 'thread-queue) thread)
  (hash-table-put! (slot-ref scheduler 'runnable-table) thread #t)
  (hash-table-delete! (slot-ref scheduler 'blocking-table) thread))

(define (%add-blocking-thread! scheduler thread timeout-time port-and-flags)
  (enqueue! (slot-ref scheduler 'thread-queue) thread)
  (hash-table-delete! (slot-ref scheduler 'runnable-table) thread)
  (hash-table-put! (slot-ref scheduler 'blocking-table)
                   thread (list timeout-time port-and-flags)))

(define (%remove-thread! scheduler thread)
  (remove-from-queue! (lambda (x) (eq? x thread)) 
                      (slot-ref scheduler 'thread-queue))
  (hash-table-delete! (slot-ref scheduler 'runnable-table) thread)
  (hash-table-delete! (slot-ref scheduler 'blocking-table) thread))

(define (%run-all-blocking-threads! scheduler)
  (hash-table-for-each (slot-ref scheduler 'blocking-table)
                       (lambda (thread param-list)
                         (hash-table-put! (slot-ref scheduler 'runnable-table)
                                          thread #t)))
  (slot-set! scheduler 'blocking-table (make-hash-table)))

(define (%hibernate scheduler)
  (when (= (hash-table-num-entries (slot-ref scheduler 'runnable-table)) 0)
    (let ((sel (make <selector>))
          (timeout-time #f))
      (hash-table-for-each (slot-ref scheduler 'blocking-table)
                           (lambda (k param-list)
                             (receive (time port-and-flags) 
                                 (apply values param-list)
                               (if (and time (or (not timeout-time)
                                                 (time<? time timeout-time)))
                                   (set! timeout-time time))
                               (if port-and-flags
                                   (apply (cut selector-add! sel <> (lambda _ #f)
                                               <>)
                                          port-and-flags)))))
      (selector-select sel 
                       (if timeout-time
                           (max 0 (* (sys-difftime timeout-time (current-time))
                                     1000000))
                           #f)))))
  
(define (%switch-thread! scheduler)
  (call/cc (lambda (cont)
             (slot-set! (%current-thread scheduler) 'cont cont)
             (%hibernate scheduler)
             (let* ((queue (slot-ref scheduler 'thread-queue))
                    (next-thread (if (queue-empty? queue)
                                     (exit 0)
                                     (dequeue! queue))))
               (slot-set! scheduler 'current-thread next-thread)
               (hash-table-delete! (slot-ref scheduler 'runnable-table) 
                                   next-thread)
               (hash-table-delete! (slot-ref scheduler 'blocking-table) 
                                   next-thread)
               ((slot-ref next-thread 'cont) #f))))
  #f)

(define (%is-thread-already-started? scheduler thread)
  (find-in-queue (cut eq? <> thread) (slot-ref scheduler 'thread-queue)))

(define %scheduler (make <scheduler>))

;;
;; thread
;;
(define (%is-terminate-thread? thread)
  (if (slot-ref thread 'result)
      #t
      #f))

(define (gauche-thread-type)
  'cthread)

(define (current-thread)
  (%current-thread %scheduler))

(define (thread? obj)
  (is-a? obj <cthread>))

(define (make-thread thunk . opts)
  (let-optionals* opts ((name #f))
    (letrec ((thread
              (make <cthread>
                :name name
                :cont (lambda _
                        (slot-set! thread 'result
                                   (guard (e (else (%make-result-exception e)))
                                          (%make-result-value (apply thunk ()))))
                        (%run-all-blocking-threads! %scheduler)
                        (%switch-thread! %scheduler))
                :params (map (lambda (kons)
                               (cons (car kons) (cdr kons)))
                             (slot-ref (current-thread) 'params)))))
      thread)))

(define (thread-name thread)
  (slot-ref thread 'name))

(define (thread-specific thread)
  (slot-ref thread 'specific))

(define (thread-specific-set! thread value)
  (slot-set! thread 'specific value))

(define (thread-start! thread)
  (if (%is-thread-already-started? %scheduler thread)
      (errorf "attempt to start an already-started thread: ~s" thread)
      (%add-runnable-thread! %scheduler thread))
  thread)

(define (thread-yield!)
  (%add-runnable-thread! %scheduler (current-thread))
  (%switch-thread! %scheduler))

(define (thread-sleep! timeout)
  (let loop ((tlimit (absolute-time timeout)))
    (if (time<? (current-time) tlimit)
        (begin
          (%add-blocking-thread! %scheduler (current-thread) tlimit #f)
          (%switch-thread! %scheduler)
          (loop tlimit))
        (undefined))))

(define (thread-terminate! thread)
  (%remove-thread! %scheduler thread)
  (slot-set! thread 'result (%make-result-exception
                             (make-condition <terminated-thread-exception>
                                             'thread (current-thread)
                                             'terminator (current-thread))))
  (if (eq? (current-thread) thread)
      (begin
        (%run-all-blocking-threads! %scheduler)
        (%switch-thread! %scheduler))
      (thread-yield!)))

(define (thread-join! thread . opts)
  (let-optionals* opts ((timeout #f) . rest)
    (let loop ((tlimit (and timeout (absolute-time timeout))))
      (cond
       ((%is-terminate-thread? thread)
        (let ((result (slot-ref thread 'result)))
          (guard (e (else (raise (make-condition <uncaught-exception>
                                                 'thread thread
                                                 'reason e))))
                 (%result-return result))))
       ((and tlimit
             (time<? tlimit (current-time)))
        (if (null? rest)
            (raise (make-condition <join-timeout-exception> 'thread thread))
            (car rest)))
       (else
        (%add-blocking-thread! %scheduler (current-thread) tlimit #f)
        (%switch-thread! %scheduler)
        (loop tlimit))))))

(define (thread-select! port flag . opts)
  (let-optionals* opts ((timeout #f))
    (let ((sel (make <selector>)))
      (selector-add! sel port (lambda _ #f) flag)
      (let loop ((tlimit (and timeout (absolute-time timeout))))
        (cond
         ((<= 1 (selector-select sel 0))
          #t)
         ((and tlimit
               (time<? tlimit (current-time)))
          #f)
         (else
          (%add-blocking-thread! %scheduler (current-thread) 
                                 tlimit (list port flag))
          (%switch-thread! %scheduler)
          (loop tlimit)))))))

;;
;; mutex and condition variable
;;

(define-class <cmutex> ()
  ((name)
   (state :init-value 'not-abandoned)
   (specific)
   (lock-waiting-thread-queue :init-form (make-queue))))

(define (mutex? obj)
  (is-a? obj <cmutex>))

(define (make-mutex . args)
  (let-optionals* args ((name #f))
    (let ((obj (make <cmutex>)))
      (if name
          (slot-set! obj 'name name))
      obj)))

(define (mutex-name mutex)
  (slot-ref mutex 'name))

(define (mutex-specific mutex)
  (slot-ref mutex 'specific))

(define (mutex-specific-set! mutex value)
  (slot-set! mutex 'specific value))

(define (mutex-state mutex)
  (slot-ref mutex 'state))

(define (%mutex-state-set! mutex state)
  (slot-set! mutex 'state state))

(define (%mutex-lock! mutex thread)
  (%mutex-state-set! mutex (or thread 'not-owned)))

(define (mutex-lock! mutex . args)
  (let-optionals* args ((timeout #f)
                        (thread (current-thread)))
    (let loop ((tlimit (and timeout (absolute-time timeout))))
      (cond
        ((eq? (mutex-state mutex) 'abandoned)
         (%mutex-lock! mutex thread)
         (raise (make-condition <abandoned-mutex-exception>
                                'thread (current-thread)
                                'mutex mutex)))
        ((eq? (mutex-state mutex) 'not-abandoned)
         (%mutex-lock! mutex thread)
         #t)
        ((and tlimit
              (time<? tlimit (current-time)))
         #f)
        (else
         (%add-blocking-thread! %scheduler (current-thread) tlimit #f)
         (%switch-thread! %scheduler)
         (loop tlimit))))))

(define (mutex-unlock! mutex . args)
  (let-optionals* args ((condition-variable #f)
                        (timeout #f))
    (slot-set! mutex 'state 'not-abandoned)
    (thread-yield!)
    (let loop ((tlimit (and timeout (absolute-time timeout))))
      (cond
       ((and (not condition-variable) (not timeout))
        #t)
       ((and condition-variable
             (eq? (slot-ref condition-variable 'signal) 'notify))
        (slot-set! condition-variable 'signal #f)
        #t)
       ((and condition-variable
             (eq? (slot-ref condition-variable 'signal) 'broadcast))
        #t)
       ((and tlimit
             (time<? tlimit (current-time)))
        #f)
       (else
        (%add-blocking-thread! %scheduler (current-thread) tlimit #f)
        (%switch-thread! %scheduler)
        (loop tlimit))))))

(define (with-locking-mutex mutex thunk)
  (dynamic-wind
      (lambda ()
        (mutex-lock! mutex))
      thunk
      (lambda ()
        (mutex-unlock! mutex))))

(define-class <ccondition-variable> ()
  ((name :init-keyword :name)
   (specific)
   (signal :init-value #f)))

(define (condition-variable? obj)
  (is-a? obj <ccondition-variable>))

(define (make-condition-variable . args)
  (let-optionals* args ((name #f))
    (make <ccondition-variable> :name name)))

(define (condition-variable-name cv)
  (slot-ref cv 'name))

(define (condition-variable-specific cv)
  (slot-ref cv 'specific))

(define (condition-variable-specific-set! cv value)
  (slot-set! cv 'specific value))

(define (condition-variable-signal! cv)
  (slot-set! cv 'signal 'notify)
  (thread-yield!)
  (slot-set! cv 'signal #f)
  #f)

(define (condition-variable-broadcast! cv)
  (slot-set! cv 'signal 'broadcast)
  (thread-yield!)
  (slot-set! cv 'signal #f)
  #f)

;;
;; conditions and a related function
;;
(define-condition-type <thread-exception> <condition>
  #f
  (thread))

(define-condition-type <terminated-thread-exception> <thread-exception>
  terminated-thread-exception?
  (terminator))

(define-condition-type <join-timeout-exception> <thread-exception>
  join-timeout-exception?)

(define-condition-type <abandoned-mutex-exception> <thread-exception>
  abandoned-mutex-exception?
  (mutex))

(define-condition-type <uncaught-exception> <thread-exception>
  uncaught-exception?
  (reason))

(define (uncaught-exception-reason exc)
  (slot-ref exc 'reason))

(provide "cthreads")

;; end of file
